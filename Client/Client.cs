﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using PadiDstmCommonTypes;

namespace Client {

    class Client {

        static void Main(string[] args) {
            //Circle(args);
            //CrossedLocks(args);
            //SampleApp(args); // Must pass in a "C" as argument when calling Client.exe
            
            /* Homemade tests*/
            //writeTest1(args);
            //writeTest2(args);
            //readTest1(args);
            //readTest2(args);
            //readTest3(args);
            //CreatePadIntsInsideTx();
            //FailPrimaryTest();
            SimpleTestOfWritesReads();
            //CoordinatorFailTest();
            //CreateSamePadintIn2TxSameTime(args);
            //StatusLoop();
            //CachedResponseTest();
            //checkBalance();
        }

        static void checkBalance()
        {
            PadiDstm.Init();
            for (uint i = 1; i < 10001; i++ )
            {
                PadiDstm.TxBegin();
                PadiDstm.CreatePadInt(i);
                PadiDstm.TxCommit();
            }
            PadiDstm.Status();
            Console.ReadLine();
        }

        /* Shows the system using cached responses to avoid repeting requests already done.
         * This test needs that the primary server fails right after sending the request to the backup.
         * The backup detects the primary failure, replace him, and uses the cache to answer the client,
         * since the client lost contact with the first primary, he resends the request to the new primary.
         */
        static void CachedResponseTest()
        {
            PadiDstm.Init();
            PadiDstm.TxBegin();
            PadInt p1 = PadiDstm.CreatePadInt(1);
            p1.Write(1000000);
            PadiDstm.TxCommit();
            PadiDstm.Status();
            Console.ReadLine();
        }

        /* Simple test that runs in loop to allow us the status of the system while we play around by
         * shut down some primary servers, backup servers, introduce some servers. 
         */
        static void StatusLoop()
        {
            PadiDstm.Init();
            while (true)
            {
                PadiDstm.Status();
                Thread.Sleep(4000);
            }
        }

        #region Timestamp Ordering Tests

        //usage: run Client.exe C
        //when prompted run Client.exe
        static void writeTest1(string[] args) {
            PadiDstm.Init();
            PadiDstm.Status();
            PadiDstm.TxBegin();

            if ((args.Length > 0) && args[0].Equals("C")) {
                PadInt p1 = PadiDstm.CreatePadInt(1);
                p1.Write(1);
                PadiDstm.TxCommit();
                PadiDstm.TxBegin();
                p1 = PadiDstm.AccessPadInt(1);
                p1.Write(2);
                PadiDstm.Status();
                Console.WriteLine("status before commit. Value should be 1 and 2 as tentative");
                Console.WriteLine("launch second client now");
                Console.ReadLine();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit. value should be 3 and no tentatives, 2 was discarded");
                Console.WriteLine("press enter to finish");
                Console.ReadLine();
                
            }
            else {
                PadInt p1 = PadiDstm.AccessPadInt(1);
                p1.Write(3);
                PadiDstm.Status();
                Console.WriteLine("status before commit. Value should be 1 and 2/3 as tentative");
                Console.WriteLine("press enter in this window");
                Console.ReadLine();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit. value should be 3 and 2 as tentative");
                Console.WriteLine(" Now press enter in the other window");
                Console.ReadLine();
            }
        }

        //usage: run Client.exe C
        //when prompted run Client.exe
        static void writeTest2(string[] args) {
            PadiDstm.Init();
            PadiDstm.Status();
            PadiDstm.TxBegin();

            if ((args.Length > 0) && args[0].Equals("C")) {
                PadInt p1 = PadiDstm.CreatePadInt(1);
                p1.Write(1);
                PadiDstm.TxCommit();
                PadiDstm.TxBegin();
                PadiDstm.Status();
                Console.WriteLine("status after begin. Value should be 1");
                Console.WriteLine("launch second client now");
                Console.ReadLine();
                p1 = PadiDstm.AccessPadInt(1);
                p1.Write(2);
                PadiDstm.Status();
                Console.WriteLine("status before commit. Value should be 3 and 2 as tentative");
                Console.ReadLine();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit. value should be 3 and no tentatives, 2 was discarded");
                Console.WriteLine("press enter to finish");
                Console.ReadLine();

            }
            else {
                PadInt p1 = PadiDstm.AccessPadInt(1);
                p1.Write(3);
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit. value should be 3");
                Console.WriteLine(" Now press enter in the other window");
                Console.ReadLine();
            }
        }

        //usage: run Client.exe C
        //when prompted run Client.exe
        static void readTest1(string[] args) {
            PadiDstm.Init();
            PadiDstm.Status();
            PadiDstm.TxBegin();

            if ((args.Length > 0) && args[0].Equals("C")) {
                PadInt p1 = PadiDstm.CreatePadInt(1);
                p1.Write(1);
                PadiDstm.TxCommit();
                PadiDstm.TxBegin();
                PadiDstm.Status();
                Console.WriteLine("status after begin. Value should be 1");
                Console.WriteLine("launch second client now");
                Console.ReadLine();
                p1 = PadiDstm.AccessPadInt(1);
                p1.Read();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit. Read value should be 1. Transaction should commit successfully");
                Console.ReadLine();
            }
            else {
                PadInt p1 = PadiDstm.AccessPadInt(1);
                p1.Write(3);
                PadiDstm.Status();
                Console.WriteLine("status before commit. value should be 1 ans 3 as tentative");
                Console.WriteLine(" Now press enter in the other window");
                Console.ReadLine();
            }
        }

        //usage: run Client.exe C
        //when prompted run Client.exe
        static void readTest2(string[] args) {
            PadiDstm.Init();
            PadiDstm.Status();
            PadiDstm.TxBegin();

            if ((args.Length > 0) && args[0].Equals("C")) {
                PadInt p1 = PadiDstm.CreatePadInt(1);
                p1.Write(1);
                PadiDstm.TxCommit();
                PadiDstm.TxBegin();
                p1 = PadiDstm.AccessPadInt(1);
                p1.Write(2);
                PadiDstm.Status();
                Console.WriteLine("status before commit. Value should be 1 and 2 as tentative");
                Console.WriteLine("launch second client now");
                Console.ReadLine();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit.");
                
            }
            else {
                PadInt p1 = PadiDstm.AccessPadInt(1);
                p1.Read();
                PadiDstm.Status();
                Console.WriteLine("status before commit. read should have failed?");
                Console.ReadLine();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit.");
            }
        }

        //usage: run Client.exe C
        //when prompted run Client.exe
        static void readTest3(string[] args) {
            PadiDstm.Init();
            PadiDstm.Status();
            PadiDstm.TxBegin();

            if ((args.Length > 0) && args[0].Equals("C")) {
                PadInt p1 = PadiDstm.CreatePadInt(1);
                p1.Write(1);
                PadiDstm.TxCommit();
                PadiDstm.TxBegin();
                PadiDstm.Status();
                Console.WriteLine("status after begin. Value should be 1");
                Console.WriteLine("launch second client now");
                Console.ReadLine();
                p1 = PadiDstm.AccessPadInt(1);
                p1.Read();
                PadiDstm.Status();
                Console.WriteLine("status before commit. Read should have failed");
                Console.ReadLine();
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit.");
                Console.ReadLine();

            }
            else {
                PadInt p1 = PadiDstm.AccessPadInt(1);
                p1.Write(3);
                PadiDstm.TxCommit();
                PadiDstm.Status();
                Console.WriteLine("status after commit. value should be 3");
                Console.WriteLine(" Now press enter in the other window");
                Console.ReadLine();
            }
        }

        #endregion

        static void CreateSamePadintIn2TxSameTime(string[] args)
        {
            PadiDstm.Init();

            PadiDstm.Status();

            PadiDstm.TxBegin();

            if ((args.Length > 0) && args[0].Equals("C")){
                PadInt p1 = PadiDstm.CreatePadInt(323);
                p1.Write(1);
                p1.Read();
            }
            else
            {
                PadInt p1 = PadiDstm.AccessPadInt(323);
                p1.Read();
            }
                

            PadiDstm.Status();

            Console.WriteLine("Press enter to commit...");
            Console.ReadLine();

            bool result = PadiDstm.TxCommit();

            PadiDstm.Status();

            string textToPrint = "This Tx ";

            if (result)
                textToPrint += "succeed!";
            else
                textToPrint += "failed!";

            textToPrint += "\nTest is over";
   
            Console.WriteLine(textToPrint);

            Console.ReadLine();
        }

        static void CoordinatorFailTest(){ //To test with 2 servers
            PadiDstm.Init();

            PadiDstm.Status();

            PadiDstm.TxBegin();
            PadInt p1 = PadiDstm.CreatePadInt(1);
            //PadInt p1 = PadiDstm.AccessPadInt(1);
            PadiDstm.Status();
            Console.WriteLine("PadInt created. Now close the primary server storing it. Press Enter...");
            Console.ReadLine();
            p1.Write(10);
            //Console.WriteLine("P1 = " +p1.Read());
            PadiDstm.Status();
            PadiDstm.TxCommit();
            PadiDstm.Status();
            Console.WriteLine("Test is over.");
            Console.ReadLine();
        }

        static void SimpleTestOfWritesReads(){
            PadiDstm.Init();

            PadiDstm.TxBegin();
            PadInt p1 = PadiDstm.CreatePadInt(1312);
            p1.Write(50);
            Console.WriteLine("New value P1 = " + p1.Read());
            PadiDstm.Status();
            p1 = PadiDstm.AccessPadInt(1312);
            p1.Write(1000000);
            Console.WriteLine("New value P1 = " + p1.Read());
            PadiDstm.Status();
            PadiDstm.TxCommit();
            PadiDstm.Status();
            Console.WriteLine("Enter to 2nd...");
            Console.ReadLine();
            PadiDstm.TxBegin();
            p1 = PadiDstm.CreatePadInt(1312);
            if (p1 == null)
                Console.WriteLine("nice!");
            else
                Console.WriteLine("not nice...");
            p1 = null;
            p1 = PadiDstm.AccessPadInt(1312);
            Console.WriteLine("New value P1 = " + p1.Read());
            p1.Write(50);
            Console.WriteLine("New value P1 = " + p1.Read());

            PadiDstm.Status();
            if (PadiDstm.TxCommit())
                Console.WriteLine("sucess");
            PadiDstm.Status();
            PadiDstm.TxBegin();
            p1 = PadiDstm.AccessPadInt(1312);
            p1.Write(-10);
            Console.WriteLine("New value P1 = " + p1.Read());
            PadiDstm.Status();
            if(PadiDstm.TxAbort())
                Console.WriteLine("sucess");
            PadiDstm.Status();
            PadiDstm.TxBegin();
            p1 = PadiDstm.AccessPadInt(1312);
            Console.WriteLine("New value P1 = " + p1.Read());
            PadiDstm.Status();
            PadiDstm.TxCommit();
            PadiDstm.Status();
        }

        static void FailPrimaryTest(){
            PadiDstm.Init();

            PadiDstm.Status();

            Console.WriteLine("Enter to continue...");
            //Turn off Primary server
            Console.ReadLine();

            PadiDstm.Status();

            Console.WriteLine("Enter to continue...");
            Console.ReadLine();
        }

        static void CreatePadIntsInsideTx(){
            PadiDstm.Init();

            PadiDstm.TxBegin();
            PadiDstm.CreatePadInt(1);
            PadiDstm.Status();
            PadiDstm.TxAbort();

            if (PadiDstm.AccessPadInt(1) == null)
                Console.WriteLine("Test 1: Success!");
            else
                Console.WriteLine("Test 1: Fail!");

            Console.WriteLine("Enter to continue...");
            Console.ReadLine();

            PadiDstm.TxBegin();
            PadiDstm.CreatePadInt(2);
            PadiDstm.Status();
            PadiDstm.TxCommit();

            if (PadiDstm.AccessPadInt(2) == null)
                Console.WriteLine("Test 2: Fail!");
            else
                Console.WriteLine("Test 2: Success!");

            Console.WriteLine("Enter to continue...");
            Console.ReadLine();

            PadiDstm.TxBegin();
            PadiDstm.CreatePadInt(3);
            PadiDstm.CreatePadInt(4);
            PadiDstm.Status();
            PadiDstm.TxAbort();

            if (PadiDstm.AccessPadInt(3) == null && PadiDstm.AccessPadInt(4) == null)
                Console.WriteLine("Test 3: Success!");
            else
                Console.WriteLine("Test 3: Fail!");

            Console.WriteLine("Enter to continue...");
            Console.ReadLine();

            PadiDstm.TxBegin();
            PadiDstm.CreatePadInt(5);
            PadiDstm.CreatePadInt(6);
            PadiDstm.Status();
            PadiDstm.TxCommit();

            if (PadiDstm.AccessPadInt(5) != null && PadiDstm.AccessPadInt(6) != null)
                Console.WriteLine("Test 4: Success!");
            else
                Console.WriteLine("Test 4: Fail!");

            Console.WriteLine("Enter to continue...");
            Console.ReadLine();

            PadiDstm.Status();

            Console.ReadLine();
        }

        static void SampleApp(string[] args){
            bool res = false;
            PadInt pi_a, pi_b;
            PadiDstm.Init();

            // Create 2 PadInts
            if ((args.Length > 0) && (args[0].Equals("C")))
            {
                try
                {
                    res = PadiDstm.TxBegin();
                    pi_a = PadiDstm.CreatePadInt(1);
                    pi_b = PadiDstm.CreatePadInt(2000000000);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("BEFORE create commit. Press enter for commit.");
                    Console.WriteLine("####################################################################");
                    PadiDstm.Status();
                    Console.ReadLine();
                    res = PadiDstm.TxCommit();
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("AFTER create commit returned " + res + " . Press enter for next transaction.");
                    Console.WriteLine("####################################################################");
                    Console.ReadLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("AFTER create ABORT. Commit returned " + res + " . Press enter for next transaction.");
                    Console.WriteLine("####################################################################");
                    Console.ReadLine();
                    PadiDstm.TxAbort();
                }
            }

            try
            {
                res = PadiDstm.TxBegin();
                pi_a = PadiDstm.AccessPadInt(1);
                pi_b = PadiDstm.AccessPadInt(2000000000);
                Console.WriteLine("####################################################################");
                Console.WriteLine("Status after AccessPadint");
                Console.WriteLine("####################################################################");
                PadiDstm.Status();
                if ((args.Length > 0) && ((args[0].Equals("C")) || (args[0].Equals("A"))))
                {
                    pi_a.Write(11);
                    pi_b.Write(12);
                }
                else
                {
                    pi_a.Write(21);
                    pi_b.Write(22);
                }
                Console.WriteLine("####################################################################");
                Console.WriteLine("Status after write. Press enter for read.");
                Console.WriteLine("####################################################################");
                PadiDstm.Status();
                Console.WriteLine("1 = " + pi_a.Read());
                Console.WriteLine("2000000000 = " + pi_b.Read());
                Console.WriteLine("####################################################################");
                Console.WriteLine("Status after read. Press enter for commit.");
                Console.WriteLine("####################################################################");
                PadiDstm.Status();
                Console.ReadLine();
                res = PadiDstm.TxCommit();
                Console.WriteLine("####################################################################");
                Console.WriteLine("Status after commit. commit = " + res + "Press enter for verification transaction.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("####################################################################");
                Console.WriteLine("AFTER r/w ABORT. Commit returned " + res + " . Press enter for abort and next transaction.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
                PadiDstm.TxAbort();
            }

            try
            {
                res = PadiDstm.TxBegin();
                PadInt pi_c = PadiDstm.AccessPadInt(1);
                PadInt pi_d = PadiDstm.AccessPadInt(2000000000);
                Console.WriteLine("####################################################################");
                Console.WriteLine("1 = " + pi_c.Read());
                Console.WriteLine("2000000000 = " + pi_d.Read());
                Console.WriteLine("Status after verification read. Press enter for commit and exit.");
                Console.WriteLine("####################################################################");
                PadiDstm.Status();
                Console.ReadLine();
                res = PadiDstm.TxCommit();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("####################################################################");
                Console.WriteLine("AFTER verification ABORT. Commit returned " + res + " . Press enter for abort and exit.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
                PadiDstm.TxAbort();
            }
        }

        static void CrossedLocks(string[] args){
            bool res = false;
            PadInt pi_a, pi_b;
            PadiDstm.Init();

            if ((args.Length > 0) && (args[0].Equals("C")))
            {
                try
                {
                    res = PadiDstm.TxBegin();
                    pi_a = PadiDstm.CreatePadInt(1);
                    pi_b = PadiDstm.CreatePadInt(2000000000);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("BEFORE create commit. Press enter for commit.");
                    Console.WriteLine("####################################################################");
                    PadiDstm.Status();
                    Console.ReadLine();
                    res = PadiDstm.TxCommit();
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("AFTER create commit. commit = " + res + " . Press enter for next transaction.");
                    Console.WriteLine("####################################################################");
                    Console.ReadLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("AFTER create ABORT. Commit returned " + res + " . Press enter for abort and next transaction.");
                    Console.WriteLine("####################################################################");
                    Console.ReadLine();
                    PadiDstm.TxAbort();
                }

            }

            try
            {
                res = PadiDstm.TxBegin();
                if ((args.Length > 0) && ((args[0].Equals("A")) || (args[0].Equals("C"))))
                {
                    pi_b = PadiDstm.AccessPadInt(2000000000);
                    pi_b.Write(211);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("Status post first op: write. Press enter for second op.");
                    Console.WriteLine("####################################################################");
                    PadiDstm.Status();
                    Console.ReadLine();
                    pi_a = PadiDstm.AccessPadInt(1);
                    //pi_a.Write(212);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("Status post second op: read. uid(1)= " + pi_a.Read() + ". Press enter for commit.");
                    Console.WriteLine("####################################################################");
                    PadiDstm.Status();
                    Console.ReadLine();
                }
                else
                {
                    pi_a = PadiDstm.AccessPadInt(1);
                    pi_a.Write(221);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("Status post first op: write. Press enter for second op.");
                    Console.WriteLine("####################################################################");
                    PadiDstm.Status();
                    Console.ReadLine();
                    pi_b = PadiDstm.AccessPadInt(2000000000);
                    //pi_b.Write(222);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("Status post second op: read. uid(1)= " + pi_b.Read() + ". Press enter for commit.");
                    Console.WriteLine("####################################################################");
                    PadiDstm.Status();
                    Console.ReadLine();
                }
                res = PadiDstm.TxCommit();
                Console.WriteLine("####################################################################");
                Console.WriteLine("commit = " + res + " . Press enter for verification transaction.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("####################################################################");
                Console.WriteLine("AFTER r/w ABORT. Commit returned " + res + " . Press enter for abort and next transaction.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
                PadiDstm.TxAbort();
            }

            try
            {
                res = PadiDstm.TxBegin();
                PadInt pi_c = PadiDstm.AccessPadInt(1);
                PadInt pi_d = PadiDstm.AccessPadInt(2000000000);
                Console.WriteLine("0 = " + pi_c.Read());
                Console.WriteLine("2000000000 = " + pi_d.Read());
                Console.WriteLine("####################################################################");
                Console.WriteLine("Status after verification read. Press enter for verification commit.");
                Console.WriteLine("####################################################################");
                PadiDstm.Status();
                res = PadiDstm.TxCommit();
                Console.WriteLine("####################################################################");
                Console.WriteLine("commit = " + res + " . Press enter for exit.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("####################################################################");
                Console.WriteLine("AFTER verification ABORT. Commit returned " + res + " . Press enter for abort and exit.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
                PadiDstm.TxAbort();
            }
        }

        static void Circle(string[] args){
            bool res = false; int aborted = 0, committed = 0;

            PadiDstm.Init();
            try
            {
                if ((args.Length > 0) && (args[0].Equals("C")))
                {
                    res = PadiDstm.TxBegin();
                    PadInt pi_a = PadiDstm.CreatePadInt(2);
                    PadInt pi_b = PadiDstm.CreatePadInt(2000000001);
                    PadInt pi_c = PadiDstm.CreatePadInt(1000000000);
                    pi_a.Write(0);
                    pi_b.Write(0);
                    res = PadiDstm.TxCommit();
                }
                Console.WriteLine("####################################################################");
                Console.WriteLine("Finished creating PadInts. Press enter for 300 R/W transaction cycle.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("####################################################################");
                Console.WriteLine("AFTER create ABORT. Commit returned " + res + " . Press enter for abort and next transaction.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
                PadiDstm.TxAbort();
            }
            for (int i = 0; i < 300; i++)
            {
                try
                {
                    res = PadiDstm.TxBegin();
                    PadInt pi_d = PadiDstm.AccessPadInt(2);
                    PadInt pi_e = PadiDstm.AccessPadInt(2000000001);
                    PadInt pi_f = PadiDstm.AccessPadInt(1000000000);
                    int d = pi_d.Read();
                    d++;
                    pi_d.Write(d);
                    int e = pi_e.Read();
                    e++;
                    pi_e.Write(e);
                    int f = pi_f.Read();
                    f++;
                    pi_f.Write(f);
                    Console.Write(".");
                    res = PadiDstm.TxCommit();
                    if (res) { committed++; Console.Write("."); }
                    else
                    {
                        aborted++;
                        Console.WriteLine("$$$$$$$$$$$$$$ ABORT $$$$$$$$$$$$$$$$$");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                    Console.WriteLine("####################################################################");
                    Console.WriteLine("AFTER create ABORT. Commit returned " + res + " . Press enter for abort and next transaction.");
                    Console.WriteLine("####################################################################");
                    Console.ReadLine();
                    PadiDstm.TxAbort();
                    aborted++;
                }

            }
            Console.WriteLine("####################################################################");
            Console.WriteLine("committed = " + committed + " ; aborted = " + aborted);
            Console.WriteLine("Status after cycle. Press enter for verification transaction.");
            Console.WriteLine("####################################################################");
            PadiDstm.Status();
            Console.ReadLine();

            try
            {
                res = PadiDstm.TxBegin();
                PadInt pi_g = PadiDstm.AccessPadInt(2);
                PadInt pi_h = PadiDstm.AccessPadInt(2000000001);
                PadInt pi_j = PadiDstm.AccessPadInt(1000000000);
                int g = pi_g.Read();
                int h = pi_h.Read();
                int j = pi_j.Read();
                res = PadiDstm.TxCommit();
                Console.WriteLine("####################################################################");
                Console.WriteLine("2 = " + g);
                Console.WriteLine("2000000001 = " + h);
                Console.WriteLine("1000000000 = " + j);
                Console.WriteLine("Status post verification transaction. Press enter for exit.");
                Console.WriteLine("####################################################################");
                PadiDstm.Status();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine("####################################################################");
                Console.WriteLine("AFTER create ABORT. Commit returned " + res + " . Press enter for abort and next transaction.");
                Console.WriteLine("####################################################################");
                Console.ReadLine();
                PadiDstm.TxAbort();
            }
        }
    }
}

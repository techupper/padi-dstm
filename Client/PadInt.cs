﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PadiDstmCommonTypes;

namespace Client {

    class PadInt {

        private uint uid;
        private ServerIF server;

        public PadInt(uint uid, ServerIF server, bool insideTx) {
            this.uid = uid;
            this.server = server;
        }

        public int Read() {

            bool retry = true;
            int tries = PadiDstmUtilities.FRONTEND_RECONECT_TRIES;

            PadiDstm.suggestCoordinator(server, uid);
            string coordinatorUrl = "";

            Console.WriteLine("[FrontEnd] Read called, padint = " + uid);

            int value = 0;
            while (retry && tries > 0){
                try{
                    coordinatorUrl = ((ServerIF)PadiDstm.TxCoordinator).getURL();
                }
                catch (Exception e){
                    Console.WriteLine("[FrontEnd] Exception catched. Probably the coordinator of transaction " + PadiDstm.currentTX + " failed:");
                    Console.WriteLine(e.Message);
                    PadiDstm.UpdateCoordinator();
                    tries--;
                }
                try{
                    value = server.ReadPadInt(uid, PadiDstm.currentTX, PadiDstm.incrNumberOfOperation(), coordinatorUrl);
                    retry = false;
                }
				catch (PadintMovedException e){
					PadiDstm.updateServerCache(uid, e.NewServer);
                    server = e.NewServer;
				    //return Read();
				}
                catch (Exception e){
                    Console.WriteLine("[FrontEnd] Exception catched. Probably the server storing PadInt " + uid + " failed:");
                    Console.WriteLine(e.Message);
                    server = PadiDstm.getServer(uid);
                    tries--;
                }
            }

            //If the frontend failed to find the server responsible for storing this padint
            if (retry)
                throw new Exception("[FrontEnd] Cannot locate server that stores padint= " + uid);
            else
                return value;
        }

        public void Write(int value) {

            bool retry = true;
            int tries = PadiDstmUtilities.FRONTEND_RECONECT_TRIES;

            PadiDstm.suggestCoordinator(server, uid);
            string coordinatorUrl = "";

            Console.WriteLine("[FrontEnd] Write called, padint = " + uid + " value = " + value);

            while (retry && tries > 0){
                try{
                    coordinatorUrl = ((ServerIF)PadiDstm.TxCoordinator).getURL();
                }
                catch (Exception e){
                    Console.WriteLine("[FrontEnd] Exception catched. Probably the coordinator of transaction " + PadiDstm.currentTX + " failed:");
                    Console.WriteLine(e.Message);
                    PadiDstm.UpdateCoordinator();
                    tries--;
                }
                try{
                    server.WritePadInt(uid, PadiDstm.currentTX, PadiDstm.incrNumberOfOperation(), value, coordinatorUrl);
                    retry = false;
                }
				catch (PadintMovedException e){
                    Console.WriteLine("[FrontEnd] PadIntMovedException catched.");
					PadiDstm.updateServerCache(uid, e.NewServer);
                    server = e.NewServer;
			    	//Write(value);
				}
                catch (Exception e){
                    Console.WriteLine("[FrontEnd] Exception catched. Probably the server storing PadInt " + uid + " failed:");
                    Console.WriteLine(e.Message);
                    server = PadiDstm.getServer(uid);
                    tries--;
                }
            }

            //If the frontend failed to find the server responsible for storing this padint
            if (retry)
                throw new Exception("[FrontEnd] Cannot locate server that stores padint= " + uid);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using PadiDstmCommonTypes;

namespace Client {

    class PadiDstm
    {
        #region Singleton

        private static readonly PadiDstm instance = new PadiDstm();

        private PadiDstm() { }

        public static PadiDstm Instance{
            get { return instance; }
        }

        #endregion

        #region State

        private static Dictionary<uint, ServerIF> cachedServerReferences;

        private static MasterIF master;

        private static bool hasCoordinator;

        public static ulong currentTX;

        private static bool underTx;

        public static ServerIF TxCoordinator;

        public static uint TxCoordinatorPadInt;

        public static ulong numberOfOperation;

        #endregion

        #region Lib Interface

        /* Called only once by the app and initializes the PADI-DSTM.
         * return: a boolean value indicating whether the operation succeeded
         */
        public static bool Init(){
            try{
                TcpChannel channel = new TcpChannel();
                ChannelServices.RegisterChannel(channel, true);

                master = (MasterIF)Activator.GetObject(
                    typeof(MasterIF),
                    PadiDstmUtilities.MSLOCATION);

                cachedServerReferences = new Dictionary<uint, ServerIF>();

                underTx = false;
            }catch (Exception e){
                Console.WriteLine("[FrontEnd] The system failed to initiate");
                Console.WriteLine("[FrontEnd]" + e.Message);
                return false;
            }
            return true;
        }

        /* Starts a new transaction.
         * return: a boolean value indicating whether the operation succeeded
         */
        public static bool TxBegin(){
            hasCoordinator = false;
            //get a transaction ID by asking a timestamp to the master
            try{
                underTx = true;
                currentTX = master.getTimestamp();
                numberOfOperation = 0;

                #if DEBUG
                Console.WriteLine("[FrontEnd] TxBegin called.");
                Console.WriteLine("[FrontEnd] Tx ID = " + currentTX);
                #endif

            }catch (Exception e){
                Console.WriteLine("[FrontEnd] PadiDstm system is inaccessible.");
                Console.WriteLine("[FrontEnd] Exception catched: " + e.Message);
                return false;
            }
            return true;
        }

        public static bool TxCommit(){
            bool result = true;

            Console.WriteLine("[FrontEnd] TxCommit called.");

            if(numberOfOperation != 0) {
                if(hasCoordinator)
                    result = TxCoordinator.closeTx(currentTX);
                else
                    throw new Exception("[FrontEnd] No coordinator was found!");
            }

            underTx = false;

            return result;
        }

        public static bool TxAbort(){
            bool result = true;

            Console.WriteLine("[FrontEnd] TxAbort called.");

            if(numberOfOperation != 0) {
                if(hasCoordinator)
                    result = TxCoordinator.abortTx(currentTX);
                else
                    throw new Exception("[FrontEnd] No coordinator was found!");
            }

            underTx = false;

            return result;
        }

        public static bool Status(){
            master.callStatus();
            return true;
        }

        public static bool Fail(string URL){

            ServerIF server = getServerRef(URL);
            if (server == null)
                return false;
            server.Fail();
            return true;

        }

        public static bool Freeze(string URL){

            ServerIF server = getServerRef(URL);
            if (server == null)
                return false;
            server.Freeze();
            return true;

        }

        public static bool Recover(string URL){

            ServerIF server = getServerRef(URL);
            if (server == null)
                return false;
            server.Recover();
            return true;

        }

        public static PadInt AccessPadInt(uint uid){

            bool retry = true;
            PadInt padint = null;
            int tries = PadiDstmUtilities.FRONTEND_RECONECT_TRIES;

            Console.WriteLine("[FrontEnd] AcessPadInt called, padint = " + uid);

            while (retry && tries > 0){
                try{
                    ServerIF server = getServer(uid);

                    if (server.CanAccessPadInt(uid, currentTX))
                        padint = new PadInt(uid, server, underTx);
                    else
                        padint = null;
                    retry = false;
                }
				catch (PadintMovedException e){
                    Console.WriteLine("[FrontEnd] PadintMovedException catched in AccessPadInt");
				    updateServerCache(uid, e.NewServer);
				    //return AccessPadInt(uid);
				}
                catch (Exception e){
                    Console.WriteLine(e.Message);
                    tries--;
                }   
            }
            return padint;
        }

        public static PadInt CreatePadInt(uint uid){

            bool retry = true;
            PadInt padint = null;
            int tries = PadiDstmUtilities.FRONTEND_RECONECT_TRIES;

            Console.WriteLine("[FrontEnd] CreatePadInt called, padint = " + uid);

            while (retry && tries > 0){

                try{
                    ServerIF server = getServer(uid);

                    if (underTx)
                        suggestCoordinator(server, uid);

                    string coordinatorUrl = ((ServerIF)PadiDstm.TxCoordinator).getURL();

                    if (server.CreatePadInt(uid, currentTX, numberOfOperation, coordinatorUrl, underTx))
                        padint = new PadInt(uid, server, underTx);
                    else{
                        //server.noDutyOnTx(currentTX);
                        padint = null;
                    }
                    retry = false;
                }
                catch (PadintMovedException e){
                    updateServerCache(uid, e.NewServer);
                   // incrNumberOfOperation();
                   // return CreatePadInt(uid);
                }
                catch (Exception){
                    tries--;
                }
            }
            incrNumberOfOperation();
            return padint;
        }

        #endregion

        #region Auxiliar Functions

        private static ServerIF getServerRef(string url) {
            try {
                return ((ServerIF)Activator.GetObject(typeof(ServerIF), url));
            }
            catch (Exception) {
                return null;
            }
        }

        public static void UpdateCoordinator(){
            TxCoordinator = getServer(TxCoordinatorPadInt);
        }

        /* Locate and get the remote reference of the server responsible for storing PadInt uid.
         */
        public static ServerIF getServer(uint uid) {
            ServerIF server;

            if (!cachedServerReferences.TryGetValue(uid, out server)) {
                server = master.LocateServer(uid);
                cachedServerReferences.Add(uid, server);
            }
            else{
                try{
                    server.HeartBeat();
                }
                catch (Exception){
                    //updates server reference
                    server = master.LocateServer(uid);
                }
            }

            return server;
        }

        public static void suggestCoordinator(ServerIF server, uint padint){
            if (!hasCoordinator){
                TxCoordinator = server;
                TxCoordinatorPadInt = padint;
                hasCoordinator = true;
                TxCoordinator.openTx(currentTX);
            }
        }

        public static ulong incrNumberOfOperation(){
            return ++numberOfOperation;
        }

        public static void updateServerCache(uint uid, ServerIF newServer)
        {
            cachedServerReferences.Remove(uid);
            cachedServerReferences.Add(uid, newServer);
        }

        #endregion
    }
}

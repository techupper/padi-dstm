﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PadiDstmCommonTypes {

    /* Master Interface
     * This is the interface provided by the Master Server
     */
    public interface MasterIF {

        ServerIF LocateServer(UInt32 UID);

        string AddServer(String master_ip, UInt16 master_port);

        void RemoveServer(String master_ip, UInt16 master_port, String slave_ip, UInt16 slave_port);

        ulong getTimestamp();

        void callStatus();

        void PairFailed(UInt64 originalHash, bool isBackup);
    }
}

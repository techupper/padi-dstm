﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Serialization;

namespace PadiDstmCommonTypes {

    public class NoServersException : System.Exception {

        public NoServersException() : base() { }
        public NoServersException(string message) : base(message) { }
        public NoServersException(string message, System.Exception inner) : base(message, inner) { }
        protected NoServersException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }

    public class ServerCollisionException : System.Exception {

        public ServerCollisionException() : base() { }
        public ServerCollisionException(string message) : base(message) { }
        public ServerCollisionException(string message, System.Exception inner) : base(message, inner) { }
        protected ServerCollisionException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }

    public class NoSuchServerException : System.Exception {

        public NoSuchServerException() : base() { }
        public NoSuchServerException(string message) : base(message) { }
        public NoSuchServerException(string message, System.Exception inner) : base(message, inner) { }
        protected NoSuchServerException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }

    public class RequestDeniedException : System.Exception
    {

        public RequestDeniedException() : base() { }
        public RequestDeniedException(string message) : base(message) { }
        public RequestDeniedException(string message, System.Exception inner) : base(message, inner) { }
        protected RequestDeniedException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }

    [Serializable]
    public class PadintMovedException : RemotingException, ISerializable
    {
        private ServerIF newServer;

        public PadintMovedException() { }

        public PadintMovedException(ServerIF newServer)
        {
            this.newServer = newServer;
        }

        public PadintMovedException(SerializationInfo info, StreamingContext context)
        {
            newServer = (ServerIF) info.GetValue("newServer", typeof(ServerIF));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("newServer", newServer);
        }
        public ServerIF NewServer
        {
            get { return newServer; }
        }
    }
}

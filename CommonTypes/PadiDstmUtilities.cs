﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace PadiDstmCommonTypes {

    public class PadiDstmUtilities {

        public static int MSPORT = 9000;
        public static string MSNAME = "master";
        public static string DEFAULT_IP = "localhost";
        public static string MSLOCATION = "tcp://" + DEFAULT_IP + ":" + MSPORT + "/" + MSNAME;
        public static string SRVNAME = "Server";

        public static int PAIR_CHECK_TIME = 2000;
        public static int FRONTEND_RECONECT_TRIES = 5;
        public static int INITIAL_PADINT_VALUE = -1;

        #if DEBUG
        /*
        public static UInt64 GetHash(string key) {
            return UInt64.Parse(key);
        }
        public static UInt64 GetServerId(string ip, UInt16 port) {
            if (ip == "192.168.1.102")
                return 1;
            else if (ip == "192.168.1.127")
                return 19;
            else if (ip == "192.168.1.10")
                return 97;
            else if (ip == "192.168.1.25")
                return 158;
            else if (ip == "192.168.1.219") // Force hash collision
                return 158;
            return 200;
        }
         */

        public static UInt64 GetHash(string key) {

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytes = Encoding.ASCII.GetBytes(key);
            bytes = md5.ComputeHash(bytes);
            return BitConverter.ToUInt64(bytes, 0);
        }

        public static UInt64 GetServerId(string ip, UInt16 port) {

            return GetHash(ip + port);
        }
        #else
        /* This is the function that knows how to hash a key.
         * The code was taken from ChordServer.ChordHash.cs from NChordLib (http://nchord.sourceforge.net/)
         * Keys can be either a server's IP address, or UIDs provided by the client.
         */
        public static UInt64 GetHash(string key)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytes = Encoding.ASCII.GetBytes(key);
            bytes = md5.ComputeHash(bytes);
            return BitConverter.ToUInt64(bytes, 0);
        }

        public static UInt64 GetServerId(string ip, UInt16 port)
        {
            return GetHash(ip + port);
        }
        #endif
    }
}

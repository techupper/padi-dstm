﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PadiDstmCommonTypes {

    /* Server Remote Interface */
    public interface ServerIF
    {
        #region Balance

        void BeginTransfer(bool isMaster);
        void EndTransfer(bool isMaster);
        void SendData(UInt64 hash_from, UInt64 hash_to, ServerIF dest);
        void ReceiveData(UInt64 hash, UInt32 id, int value);

        #endregion

        #region Replication

        void HeartBeat();

        UInt64 RegisterBackup(string backupUrl);

        String ReplicateState();

        void NotifyPadintTransfered(uint uid, ServerIF newServer);

        #endregion

        #region PadInt's Manipulation
        bool CanAccessPadInt(uint uid, ulong txId);

        bool CreatePadInt(uint uid, ulong txId, ulong numberOfOperations, string coordinatorUrl, bool underTx);

        void WritePadInt(uint uid, ulong txId, ulong numberOfOperations, int value, string coordinatorUrl);

        int ReadPadInt(uint uid, ulong txId, ulong numberOfOperations, string coordinatorUrl);

        void printStatus();

        void Fail();

        void Freeze(); 

        void Recover();

        string getURL();
        #endregion

        #region Coordinator
        void markTxFailed(ulong txId);
        void noDutyOnTx(ulong txId);
        void openTx(ulong tx);
        bool closeTx(ulong tx);
        bool abortTx(ulong tx);
        void joinTx(ulong tx, string participantUrl);
        void addPadintPartcipant(ulong txId, UInt32 padint);
        #endregion

        #region Two Phase Commit
        bool canCommit(ulong txId);
        void doCommit(ulong txId, List<UInt32> padintParticipants);
        void doAbort(ulong txId, List<UInt32> padintParticipants);
        void haveCommitedAll(ulong txId, ServerIF participant);
        bool getDecision(ulong txId);
        #endregion

        UInt64 getOriginalHash();

        void setOriginalHash(UInt64 h);
    }
}

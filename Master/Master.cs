﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PadiDstmCommonTypes;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace PadiDstm {

    class Master {

        static void Main(string[] args) {
            registerMaster();

            //Tests
            //TestAddLocateAndRemove();
            //TestExceptions();

            Console.ReadLine();
        }

        public static void registerMaster() {

            TcpChannel channel = new TcpChannel(PadiDstmUtilities.MSPORT);
            ChannelServices.RegisterChannel(channel, true);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(MasterImpl),
                PadiDstmUtilities.MSNAME,
                WellKnownObjectMode.Singleton);
            Console.WriteLine("Master initialized.");
            Console.WriteLine("I am the Master (of Disaster!)\n");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PadiDstmCommonTypes;
using C5;
using System.Threading;
using timestamp = System.UInt64;

namespace PadiDstm {

    class MasterImpl : MarshalByRefObject, MasterIF {

        private TreeDictionary<UInt64, ServerDescriptor> routing_table;
        private Queue<ServerDescriptor> missingBackup;

        //counter used to ensure order between servers [timestamp ordering]
        private timestamp transactionCounter;
        private object txCounterLock;

        public MasterImpl() {

            routing_table = new TreeDictionary<UInt64, ServerDescriptor>();
            transactionCounter = 0;
            txCounterLock = new object();
            missingBackup = new Queue<ServerDescriptor>();
        }

        public override object InitializeLifetimeService() { return null; }

        public ServerIF LocateServer(UInt32 UID) {

            Monitor.Enter(routing_table);
            if (routing_table.Count == 0) {
                Monitor.Exit(routing_table);
                throw new NoServersException();
            }

            C5.KeyValuePair<UInt64, ServerDescriptor> res = LocateInRing(PadiDstmUtilities.GetHash(UID.ToString()));
            Monitor.Exit(routing_table);
            #if DEBUG
            Console.WriteLine("Request with UID = " + UID + " (hash: " + PadiDstmUtilities.GetHash(UID.ToString()) + ")");
            Console.WriteLine("    result = " + res.Value.Ip + "," + res.Value.Port);
            #endif
            return res.Value.Server;
        }

        public string AddServer(String ip, UInt16 port) {

            Monitor.Enter(missingBackup);
            if (missingBackup.Count > 0){
                //is added to the system as secondary
                ServerDescriptor s = missingBackup.Dequeue();
                s.Backup_ip = ip;
                s.Backup_Port = port;
                //original hash will be passed to the backup when this little guy notifies the primary about its existence
                Monitor.Exit(missingBackup);
                return "tcp://" + PadiDstmUtilities.DEFAULT_IP + ":" + s.Port + "/" + PadiDstmUtilities.SRVNAME;
            }

            ServerDescriptor new_server_descriptor = new ServerDescriptor(ip, port);
            missingBackup.Enqueue(new_server_descriptor);
            Monitor.Exit(missingBackup);

            new_server_descriptor.Server.setOriginalHash(new_server_descriptor.OriginalHash);

            UInt64 to_insert = new_server_descriptor.OriginalHash;
            #if DEBUG
            Console.WriteLine("Adding server " + ip + ", " + port + "; hash = " + to_insert);
            #endif
            Monitor.Enter(routing_table);

            if (routing_table.Count == 0) {
                routing_table.Add(to_insert + 1, new_server_descriptor);
                Monitor.Exit(routing_table);
                return "";
            }

            C5.KeyValuePair<UInt64, ServerDescriptor> successor = LocateInRing(to_insert);
            if (to_insert == successor.Value.OriginalHash) {
                /* Sorry man, no space for you. Get moving. */
                Monitor.Exit(routing_table);
                throw new ServerCollisionException();
            }

            routing_table.Remove(successor.Key);
            routing_table.Add(to_insert + 1, successor.Value);
            routing_table.Add(successor.Key, new_server_descriptor);
            // Transfer keys belonging to the newborn server
            TransferKeys(successor.Key, to_insert, successor.Value, new_server_descriptor);
            Monitor.Exit(routing_table);
            return "";
        }

        public void RemoveServer(String master_ip, UInt16 master_port, String slave_ip, UInt16 slave_port) {

            UInt64 hash = PadiDstmUtilities.GetServerId(master_ip, master_port);
            Monitor.Enter(routing_table);
            C5.KeyValuePair<UInt64, ServerDescriptor> predecessor = LocateInRing(hash);
            #if DEBUG
            Console.WriteLine("Removing server " + master_ip + ", " + master_port + "; hash = " + hash);
            #endif

            if (PadiDstmUtilities.GetServerId(predecessor.Value.Ip, predecessor.Value.Port) != hash) {
                /* Attempted to remove server that doesn't belong to circle */
                Monitor.Exit(routing_table);
                throw new NoSuchServerException();
            }

            C5.KeyValuePair<UInt64, ServerDescriptor> successor = LocateInRing(hash + 1);
            #if DEBUG
            Console.WriteLine("    action: " + predecessor.Key + " -> <" + successor.Value.Ip + "," + successor.Value.Port + ">");
            #endif
            routing_table.Remove(successor.Key);
            routing_table.Remove(predecessor.Key);
            if (predecessor != successor) {
                TransferKeys(predecessor.Key, hash, predecessor.Value, successor.Value);
                predecessor.Value = successor.Value;
                routing_table.Add(predecessor.Key, predecessor.Value);

            }
            Monitor.Exit(routing_table);
        }

        #if DEBUG
        public void PrintRing() {

            Console.WriteLine("*** Current ring status:");
            foreach (C5.KeyValuePair<UInt64, ServerDescriptor> entry in routing_table) {
                Console.WriteLine("[" + entry.Key + "," + PadiDstmUtilities.GetServerId(entry.Value.Ip, entry.Value.Port) + "] -> " + entry.Value.Ip + "," + entry.Value.Port);
            }
            Console.WriteLine("*** Finished showing ring.");
        }
        #endif

        /* transactions are ordered by their transaction ID, which is simply a timestamp returned by the Master */
        public timestamp getTimestamp() {

            Monitor.Enter(txCounterLock);
            timestamp res = ++transactionCounter;
            Monitor.Exit(txCounterLock);
            return res;
        }

        private C5.KeyValuePair<UInt64, ServerDescriptor> LocateInRing(UInt64 hash) {

            try {
                return routing_table.WeakPredecessor(hash);
            }
            catch (NoSuchItemException) {
                return routing_table.FindMax();
            }
        }

        private void TransferKeys(UInt64 hash_from, UInt64 hash_to, ServerDescriptor src, ServerDescriptor dst) {
            Console.WriteLine("Entered TransferKeys [" + hash_from + " .. " + hash_to + "]");
            src.Server.BeginTransfer(true);
            Console.WriteLine("src started transfer");
            dst.Server.BeginTransfer(true);
            Console.WriteLine("dst started transfer.");
            src.Server.SendData(hash_from, hash_to, dst.Server);
            src.Server.EndTransfer(true);
            Console.WriteLine("src end");
            dst.Server.EndTransfer(true);
            Console.WriteLine("dst end");
        }

        public void callStatus() {
            dump();

            foreach (C5.KeyValuePair<UInt64, ServerDescriptor> pair in routing_table) {
                pair.Value.Server.printStatus();
            }
        }

        void dump() {
            System.Console.WriteLine("------------------------\n----Master's Status-----\n------------------------");
            Console.WriteLine("Stored servers:");
            foreach (C5.KeyValuePair<UInt64, ServerDescriptor> pair in routing_table) {
                Console.WriteLine("\t " + pair.Value.Ip + pair.Value.Port);
            }

            if (missingBackup.Count > 0){
                Console.WriteLine("\nServers missing backup: ");
                foreach (ServerDescriptor sd in missingBackup){
                    Console.WriteLine("\t tcp://" + sd.Ip + ":" + sd.Port + "/" + PadiDstmUtilities.SRVNAME);
                }
            }
            
            System.Console.WriteLine("------------------------\n");
            
            return;
        }

        /* Informs the master that the pair server has failed. isBackup tells about the server that failed, not about the requester
         */
        public void PairFailed(UInt64 originalHash, bool isBackup)
        {

            #if DEBUG
            if(isBackup)
                System.Console.WriteLine("Backup has failed with original hash = " + originalHash);
            else
                System.Console.WriteLine("Primary has failed with original hash = " + originalHash);
            #endif

            Monitor.Enter(routing_table);
            C5.KeyValuePair<UInt64, ServerDescriptor> res = LocateInRing(originalHash);
            missingBackup.Enqueue(res.Value);

            //if the primary failed, then I need to appoint a new primary
            if(!isBackup)
                res.Value.SwitchActiveServer();
            
            Monitor.Exit(routing_table);
        }
    }
}

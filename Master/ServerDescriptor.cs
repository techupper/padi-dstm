﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PadiDstmCommonTypes;

namespace PadiDstm {

    class ServerDescriptor {

        private String primary_ip;
        private UInt16 primary_port;

        private String backup_ip;

        private UInt16 backup_port;

        private bool hasBackup;

        private UInt64 originalHash;

        ServerIF active_server;

        public ServerDescriptor(String ip, UInt16 port) {

            primary_ip = ip;
            primary_port = port;
            hasBackup = false;
            originalHash = PadiDstmUtilities.GetServerId(ip, port);
            active_server = (ServerIF)Activator.GetObject(
                typeof(ServerIF), 
                "tcp://" + PadiDstmUtilities.DEFAULT_IP + ":" + port + "/" + PadiDstmUtilities.SRVNAME);
        }

        /* The primary has failed, so the secondary is going to be the new primary server
         */
        public void SwitchActiveServer(){

            active_server = (ServerIF)Activator.GetObject(
                typeof(ServerIF), 
                "tcp://" + PadiDstmUtilities.DEFAULT_IP + ":" + backup_port + "/" + PadiDstmUtilities.SRVNAME);

            primary_ip = backup_ip;
            backup_ip = "";
            primary_port = backup_port;
            backup_port = 0;
            hasBackup = false;
        }

        public UInt16 Port {
            get { return primary_port; }
        }

        public String Ip {
            get { return primary_ip; }
        }

        public ServerIF Server {
            get { return active_server; }
        }

        public bool HasBackup {
            get { return hasBackup; }
        }

        public UInt16 Backup_Port{
            get { return backup_port; }
            set { backup_port = value; }
        }

        public String Backup_ip
        {
            get { return backup_ip; }
            set { backup_ip = value; }
        }

        public UInt64 OriginalHash
        {
            get { return originalHash; }
            set { originalHash = value; }
        } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server {

    public class WriteFailedException : System.Exception {
        public WriteFailedException() : base() { }
    }

    public class ReadFailedException : System.Exception {
        public ReadFailedException() : base() { }
    }
}

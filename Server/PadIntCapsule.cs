﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using timestamp = System.UInt64;

namespace Server {

    /**
     * PadIntCapsule class
     * Holds the basic element of the system - the PadInt. This class provides the atributes needed to implement 
     * Timestamp Ordering Algorithm.
     * It also provides the basic methods to access and manipulate the value of object.
     */
    public class PadIntCapsule {

        /* PadInt ID*/
        private uint uid;
        private int value;

        public bool initialized;

        private object mutex;

        private timestamp creatingTransaction;
        private bool creationCommited;

        /* Values needed to implement Timestamp Ordering Algorithm*/
        public timestamp lastCommitedWrite = 0;
        private SortedList<timestamp, int> tentatives; 
        public timestamp lastCommitedRead = 0;
        private List<timestamp> uncommitedReaders;

        public uint UID {
            get { return uid; }
        }

        public int Value
        {
            get { return value; }
        }

        public PadIntCapsule(uint uid) {

            this.uid = uid;
            tentatives = new SortedList<timestamp, int>();
            uncommitedReaders = new List<timestamp>();
            initialized = false;
            mutex = new object();
        }

        public PadIntCapsule(uint uid, timestamp ts) :this(uid){
            creatingTransaction = ts;
            creationCommited = false;
        }

        // Do NOT use. This constructor is only used when a PadInt is copied to a new server.
        public PadIntCapsule(uint uid, int value) : this(uid)
        {
            this.value = value;
            creationCommited = true;
        }

        public void write(int value, timestamp ts) {

            if(!creationCommited && creatingTransaction != ts)
                throw new WriteFailedException();

            if (!initialized)
                initialized = true;

            if (checkIfReadBiggerThan(ts))
                throw new WriteFailedException();

            if (lastCommitedWrite > ts)
                return; //do nothing.

            tentatives[ts] = value;
        }

        public int read(timestamp ts) {

            if (!initialized || (!creationCommited && creatingTransaction != ts)) {
                throw new ReadFailedException();
            }

            if (tentatives.ContainsKey(ts)) {
                uncommitedReaders.Add(ts);
                return tentatives[ts];
            }

            if (checkIfWriteBiggerThan(ts)) {
                throw new ReadFailedException();
            }

            uncommitedReaders.Add(ts);
            return value;
        }

        public void commit(timestamp ts) { //this should be atomic? not sure how to do it

            bool containRead = uncommitedReaders.Contains(ts);
            bool containWrite = tentatives.ContainsKey(ts);

            if (!containRead && !containWrite)
                return;

            Monitor.Enter(mutex);
            creationCommited = true;

            if (containRead) {
                lastCommitedRead = ts;
                while (uncommitedReaders.Contains(ts))
                    uncommitedReaders.Remove(ts);
            }

            if (containWrite) {    
                if (lastCommitedWrite < ts) {
                    lastCommitedWrite = ts;
                    value = tentatives[ts];
                }
                tentatives.Remove(ts);
            }
            Monitor.Exit(mutex);
        }

        public void abort(timestamp ts) {

            bool containRead = uncommitedReaders.Contains(ts);
            bool containWrite = tentatives.ContainsKey(ts);

            if (!containRead && !containWrite)
                return;

            Monitor.Enter(mutex);
            if (containRead) {
                while (uncommitedReaders.Contains(ts))
                    uncommitedReaders.Remove(ts);
            }

            if (containWrite) {
                tentatives.Remove(ts);
            }
            Monitor.Exit(mutex);
        }

        public bool checkIfAccessible(ulong txId){
            if (creationCommited)
                return true;

            return creatingTransaction == txId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ts"></param>
        /// <returns>true if padint was read by any transaction bigger than ts, else false</returns>
        private bool checkIfReadBiggerThan(timestamp ts) {
            return uncommitedReaders.Any(z => z > ts) 
                    || lastCommitedRead > ts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ts"></param>
        /// <returns>true if padint was writen by any transaction bigger than ts, else false</returns>
        private bool checkIfWriteBiggerThan(timestamp ts) {

            bool result = false;
            foreach (KeyValuePair<timestamp, int> pair in tentatives) {
                if (pair.Key < ts)
                    result = true;
            }

            return result || lastCommitedWrite > ts;
        }

        public void dump() {

            Console.WriteLine("Uid: " + UID + " Commited value: " + value);
            
            Console.Write("Last commited write ts: " + lastCommitedWrite + " | Tentative Writes (ts, v): ");
            foreach (KeyValuePair<timestamp, int> pair in tentatives) {
                Console.Write("(" + pair.Key + "," + pair.Value + ") ");
            }
            Console.Write(Environment.NewLine);

            Console.Write("Last commited read ts: " + lastCommitedRead + " | Tentative Reads ts: ");
            foreach (timestamp ts in uncommitedReaders) {
                Console.Write( ts + " ");
            }
            Console.Write(Environment.NewLine);

        }
    }
}

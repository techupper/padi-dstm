﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using PadiDstmCommonTypes;

namespace Server {

    class Server {

        private static int port;
       
        static void Main(string[] args) {

            Console.WriteLine("Server running...");

            if (args.Length < 1) {
                Console.WriteLine("Error: expected port number.");
                return;
            }
            Console.WriteLine("Port: " + args[0]);
            port = int.Parse(args[0]);

            ServerImpl server = RegisterServer();

            MasterIF master_server = (MasterIF)Activator.GetObject(typeof(MasterIF), PadiDstmUtilities.MSLOCATION);
            
            string url = master_server.AddServer(PadiDstmUtilities.DEFAULT_IP, (UInt16)port);

            //Console.WriteLine("Master.AddServer returned " + url);

            if (url.CompareTo("") != 0){
                //this is a backup server
                Console.WriteLine("Initializing as backup server for " + url);
                server.IsBackup = true;
                server.NotifyPrimary(url);
            }
            else{
                // this is primary, master already set our originalHash
                Console.WriteLine("Initializing as primary.");
                server.IsBackup = false;
            }

            Console.ReadLine();
        }

        private static ServerImpl RegisterServer() {

            BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = TypeFilterLevel.Full;
            Hashtable props = new System.Collections.Hashtable();
            props["port"] = port;
            TcpChannel channel = new TcpChannel(props, null, provider);
            ChannelServices.RegisterChannel(channel, true);

            ServerImpl server = new ServerImpl("tcp://" + PadiDstmUtilities.DEFAULT_IP + ":" + port + "/" + PadiDstmUtilities.SRVNAME);

            RemotingServices.Marshal(server,
                PadiDstmUtilities.SRVNAME,
                typeof(ServerImpl));

            return server;
        }
    }
}

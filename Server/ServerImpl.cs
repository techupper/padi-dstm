﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using PadiDstmCommonTypes;
using C5;

namespace Server {

    public class ServerImpl : MarshalByRefObject, ServerIF
    {

        #region Server's State

        private string url;
        private MasterIF master;
        private ServerIF myRemoteReference;
        /* Local PadInt objects */
        private TreeDictionary<UInt64, PadIntCapsule> storedObjects;
        /* Transactions where this server is coordinator*/
        private Dictionary<ulong, Transaction> transactions;
        /* Transactions where this server participates */
        private List<ulong> participantTransactions;
        /* Requests per transaction, allowing us to know if a request was already done
         * Each entry in the dictionary is of type <TransactionID, ListOfNumberOfOperationsDone>
         */
        private Dictionary<ulong, Dictionary<ulong, int>> requests;
        enum serverState {Alive, Fail, Freeze};
        private serverState status;
        private Object statusLock;

        private Object transferQueue;
        private int transfersCount; /* transfers on hold + active transfers (active transfers is at most 1) */

        /* Padints that were once here but are now in other servers */
        private Dictionary<uint, TransferedPadint> transferedPadints;

        private bool isBackup;

        public bool IsBackup
        {
            get { return isBackup; }
            set { isBackup = value; }
        }
        private bool hasBackup;

        /* if this.isBackup, it's the primary server's reference, otherwise it's the backup server's reference*/
        private ServerIF pairServer;
        private string pairUrl;

        private Boolean denyNewTx; // if true, new transaction requests are denied

        private Dictionary<ulong, List<UInt32>> padintsCreatedUndexTx;

        private System.Timers.Timer pairTimer;

        private UInt64 originalHash;

        public UInt64 getOriginalHash()
        {
            return originalHash;
        }

        public void setOriginalHash(UInt64 h){
            originalHash = h;
        }

        #endregion 

        public ServerImpl(string URL) {
            this.url = URL;
            status = serverState.Alive;
            storedObjects = new TreeDictionary<UInt64, PadIntCapsule>();
            transactions = new Dictionary<ulong, Transaction>();
            participantTransactions = new List<ulong>();
            myRemoteReference = (ServerIF)Activator.GetObject(typeof(ServerIF), url);
            statusLock = new Object();
            denyNewTx = false;
            padintsCreatedUndexTx = new Dictionary<ulong, List<uint>>();
            //this.isBackup = isBackup;
            master = getMasterRef();
            hasBackup = false;
            requests = new Dictionary<ulong,Dictionary<ulong,int>>();
            transferedPadints = new Dictionary<uint, TransferedPadint>();
            transferQueue = new Object();
            transfersCount = 0;
        }

        public override object InitializeLifetimeService() { return null; }

        #region Balance

        public void BeginTransfer(bool isMaster)
        {
            /* Wait for ongoing transfers (if they exist) */
            Monitor.Enter(transferQueue);
            if (transfersCount++ > 0)
            {
                Monitor.Wait(transferQueue);
            }
            Monitor.Exit(transferQueue);

            /* Do the same for backup */
            if (isMaster && !isBackup && hasBackup)
                pairServer.BeginTransfer(false);

            DenyIncomingTx();
            WaitForActiveTx();
        }

        public void EndTransfer(bool isMaster)
        {
            Monitor.Enter(participantTransactions);
            denyNewTx = false;
            Monitor.Exit(participantTransactions);

            Monitor.Enter(transferQueue);
            if (transfersCount-- > 1)
            {
                Monitor.Pulse(transferQueue);
            }
            Monitor.Exit(transferQueue);
            if (isMaster && !isBackup && hasBackup)
                pairServer.EndTransfer(false);
        }

        /* After calling this method, every new transaction request is denied by the server by
         * throwing a RequestDeniedException.
         */
        private void DenyIncomingTx()
        {
            Monitor.Enter(participantTransactions);
            denyNewTx = true;
            Monitor.Exit(participantTransactions);
        }


        /* This method is used by MasterImpl/TransferKeys() together with DenyIncomingTx() to empty the active / pending
         * transactions queue. It returns as soon as every transaction on hold finishes.
         * THIS IS REALLY IMPORTANT: this method assumes that DenyIncomingTx() has been called before.
         */
        private void WaitForActiveTx()
        {
            Monitor.Enter(participantTransactions);
            if (participantTransactions.Count == 0)
            {
                Monitor.Exit(participantTransactions);
                return;
            }
            // At least one active transaction - wait!
            Monitor.Wait(participantTransactions);
            /* When we get here, no more transactions are on hold. This is because
             * doCommit() and doAbort() call Monitor.Pulse() when they are invoked in the context
             * of the last transaction. It is safe to return.
             */
            Monitor.Exit(participantTransactions);
        }

        public bool CanContinue(ulong txId)
        {
            Monitor.Enter(participantTransactions);
            bool res = (!participantTransactions.Contains(txId) && denyNewTx);
            Monitor.Exit(participantTransactions);
            return !res;
        }

        /* Sends data to a new server. Assumes BeginTransfer() has been called on both endpoints */
        public void SendData(UInt64 hash_from, UInt64 hash_to, ServerIF dest)
        {
            if (hash_to < hash_from)
            {
                Monitor.Enter(storedObjects);
                UInt64 max;
                try
                {
                    max = storedObjects.FindMax().Key;
                }
                catch (NoSuchItemException)
                {
                    return;
                }
                finally
                {
                    Monitor.Exit(storedObjects);
                }
                if (max >= hash_from)
                {
                    // Yes, the check is necessary, to avoid infinite recursion. Think about it :)
                    SendData(hash_from, max, dest);
                }
                hash_from = 0;
            }
            UInt64 next = hash_from;
            PadIntCapsule next_padint = null;
            Monitor.Enter(storedObjects);
            try
            {
                C5.KeyValuePair<UInt64, PadIntCapsule> nextItem = storedObjects.WeakSuccessor(next);
                next = nextItem.Key;
                next_padint = nextItem.Value;
                while (next <= hash_to)
                {
                    #if DEBUG
                    Console.WriteLine("Sending PadInt " + next_padint.UID + " to new server (value = " + next_padint.Value + "; hash = " + next + ")");
                    #endif
                    dest.ReceiveData(next, next_padint.UID, next_padint.Value);
                    storedObjects.Remove(next);
                    Monitor.Enter(transferedPadints);
                    transferedPadints.Add(next_padint.UID, new TransferedPadint(next_padint.UID, dest));
                    Monitor.Exit(transferedPadints);
                    /* Do the same for the replica, i.e., signalize that this PadInt was moved to another server.
                     * At this point, it is guaranteed that BeginTransfer() has been called on the replica, and has successfully returned.
                     */
                    if (!isBackup && hasBackup)
                        pairServer.NotifyPadintTransfered(next_padint.UID, dest);
                    nextItem = storedObjects.WeakSuccessor(next + 1);
                    next = nextItem.Key;
                    next_padint = nextItem.Value;
                }
            }
            catch (NoSuchItemException)
            {
                /* Intentionally left blank
                    * If we get here, it means that storedObjects.FindMax().Key < hash_to (otherwise we would have fallen out of the while),
                    * which implicitly terminates the copy.
                    */
            }
            finally
            {
                Monitor.Exit(storedObjects);
            }
        }

        /* Receives data from a server. Assumes BeginTransfer() has been called on both endpoints. */
        public void ReceiveData(UInt64 hash, UInt32 id, int value)
        {
            #if DEBUG
            Console.WriteLine("Received new PadInt " + id + " (value = " + value + "; hash = " + hash + ")");
            #endif
            Monitor.Enter(storedObjects);
            storedObjects.Add(hash, new PadIntCapsule(id, value));
            Monitor.Exit(storedObjects);
            if (!isBackup && hasBackup)
                pairServer.ReceiveData(hash, id, value);
        }

#endregion

        #region Replication

        /* This simplistic function is used as a keep-alive message to detect if a server is faulty.
         */
        public void HeartBeat() { return; }

        /* Called by the primary on a secondary server for each transfered PadInt during an ongoing transfer. 
           This is basically a primitive way to replicate the transferedPadInts cache on the replica.
         */
        public void NotifyPadintTransfered(uint UID, ServerIF newServer) {
            Monitor.Enter(storedObjects);
            storedObjects.Remove(UID);
            Monitor.Exit(storedObjects);
            Monitor.Enter(transferedPadints);
            transferedPadints.Add(UID, new TransferedPadint(UID, newServer));
            Monitor.Exit(transferedPadints);
            #if DEBUG
            Console.WriteLine("Order, and I'll obey! -> Removed PadInt " + UID + " because primary asked me to do so.");
            #endif
        }

        /* IMPORTANT: Assumes BeginTransfer() has been called before on this server */
        public String ReplicateState()
        {
            StringBuilder res = new StringBuilder();
            Monitor.Enter(storedObjects);
            foreach (C5.KeyValuePair<UInt64, PadIntCapsule> padint in storedObjects) {
                res.Append(padint.Key + "," + padint.Value.UID + "," + padint.Value.Value + ";");
            }
            Monitor.Exit(storedObjects);
            return res.ToString();
        }

        /* The server calling this method (backup) is registering its existence with the primary server
         */
        public UInt64 RegisterBackup(string backupUrl) {

            #if DEBUG
            Console.WriteLine("[Replication]I am registering backup information...START");
            #endif

            try
            {
                pairServer = getServerRef(backupUrl);
                pairUrl = backupUrl;
                hasBackup = true;

                #if DEBUG
                Console.WriteLine("[Replication]I am registering backup information...OK");
                #endif

                //starting backup checking
                StartPairTimer();
            }
            catch (System.Net.Sockets.SocketException)
            {
                #if DEBUG
                Console.WriteLine("[Replication]I am registering backup information...FAIL");
                Console.WriteLine("[Replication]Cannot find backup at " + backupUrl);
                #endif
            }
            catch (Exception e){
                #if DEBUG
                Console.WriteLine("[Replication]I am registering backup information...FAIL");
                Console.WriteLine("[Replication]" + e.Message);
                #endif
            }

            return originalHash;
        }

        private void StateDeserialize(String state)
        {
            #if DEBUG
            if(state.CompareTo("") == 0)
                Console.WriteLine("State received: [EMPTY_STATE]");
            else
                Console.WriteLine("State received: " + state);
            #endif
            
            string[] stateFields = state.Split(new Char[] { ';' });
            foreach (string padintTuple in stateFields) {
                string[] padintInfo = padintTuple.Split(new Char[] { ',' });
                if (padintInfo.Length == 3)
                {
                    UInt64 hash = UInt64.Parse(padintInfo[0]);
                    uint uid = uint.Parse(padintInfo[1]);
                    int value = int.Parse(padintInfo[2]);
                    storedObjects.Add(hash, new PadIntCapsule(uid, value));
                }
            }
        }

        /* Inform the primary server about its backup server
         */
        public void NotifyPrimary(string primaryUrl){

            #if DEBUG
            Console.WriteLine("[Replication]I am notifying primary about my existence...START");
            #endif

            bool success = true;
            this.pairUrl = primaryUrl;

            try{
                pairServer = getServerRef(primaryUrl);
                originalHash = pairServer.RegisterBackup(url);
                pairServer.BeginTransfer(false);
                BeginTransfer(false);
                String padints = pairServer.ReplicateState();
                Monitor.Enter(storedObjects);
                StateDeserialize(padints);
                Monitor.Exit(storedObjects);
                EndTransfer(false);
                pairServer.EndTransfer(false);
            }
            catch (System.Net.Sockets.SocketException){
                success = false;
                #if DEBUG
                Console.WriteLine("[Replication]I am notifying primary about my existence...FAIL");
                #endif
            }
            catch (Exception e){
                success = false;
                #if DEBUG
                Console.WriteLine("[Replication]I am notifying primary about my existence...FAIL");
                Console.WriteLine("[Replication]" + e.Message);    
                #endif
            }
         
            //If cannot locate the primary server
            if (!success){
                BecomePrimary();
                return;
            }

            #if DEBUG
            Console.WriteLine("[Replication]I am notifying primary about my existence...OK");
            #endif

            StartPairTimer();
        }

        private void BecomePrimary(){

            #if DEBUG
            Console.WriteLine("[Replication]I am becoming the Primary Server. Nobody can stop me now!");
            #endif

            master.PairFailed(originalHash, !isBackup);
            isBackup = false;
            hasBackup = false;

            Monitor.Enter(transactions);
            foreach (System.Collections.Generic.KeyValuePair<ulong, Transaction> txPack in transactions)
            {
                txPack.Value.RemoveParticipant(pairServer); //removing the failed primary
            }
            Monitor.Exit(transactions);

            printStatus();
        }


        public void StartPairTimer()
        {
            #if DEBUG
            if(isBackup)
                Console.WriteLine("[Replication]I am starting to check recurrently primary's heartbeat.");
            else
                Console.WriteLine("[Replication]I am starting to check recurrently backup's heartbeat.");
            #endif

            pairTimer = new System.Timers.Timer(PadiDstmUtilities.PAIR_CHECK_TIME);
            pairTimer.Elapsed += CheckPair;
            pairTimer.Enabled = true;
        }

        private void CheckPair(Object source, ElapsedEventArgs e){

            pairTimer.Enabled = false;
            try{
                pairServer.HeartBeat();
                pairTimer.Enabled = true;
            }
            catch (Exception){
                if (isBackup){
                    #if DEBUG
                    Console.WriteLine("[Replication]FAILED to check primary's heartbeat.");
                    #endif
                    BecomePrimary();
                }
                else{
                    #if DEBUG
                    Console.WriteLine("[Replication]FAILED to check backup's heartbeat.");
                    #endif
                    hasBackup = false;
                    master.PairFailed(originalHash, !isBackup);
                }
                
            }
        }

        #endregion

        #region PadInt's Manipulation

        public bool CanAccessPadInt(uint uid, ulong txId)
        {
            if (!checkInternalStatus()) {
                return false;
            }
						
            checkPadintMigration(uid);
            
			Monitor.Enter(storedObjects);
            ulong key = PadiDstmUtilities.GetHash(uid.ToString());
            bool res = storedObjects.Contains(key);
            if (res)
                res = storedObjects[key].checkIfAccessible(txId);
            Monitor.Exit(storedObjects);
            endWorkBatch();
            return res;
        }

        /* Return true if the padint was created, false if it already exists.
         */
        public bool CreatePadInt(UInt32 uid, ulong txId, ulong numberOfOperations, string coordinatorUrl, bool underTx) {

            if (!isBackup && !hasBackup)
            {
                #if DEBUG
                Console.WriteLine("[Storage] Cannot guarantee fault tolerance.");
                #endif
            }

            #if DEBUG
            Console.WriteLine("[Storage] Creating padint " + uid + " START");
            #endif

            checkPadintMigration(uid);

            ServerIF coordinator = getServerRef(coordinatorUrl);

            if (!checkInternalStatus()) {
                coordinator.markTxFailed(txId);
                #if DEBUG
                Console.WriteLine("[Storage] Creating padint " + uid + " FAILED");
                #endif
                return false;
            }
            if (!CanContinue(txId))
                throw new RequestDeniedException();

            try {
                Monitor.Enter(storedObjects);
                if (!storedObjects.Contains(PadiDstmUtilities.GetHash(uid.ToString()))){
                    storedObjects.Add(PadiDstmUtilities.GetHash(uid.ToString()), new PadIntCapsule(uid, txId));

                    //If this server isn't the coordinator
                    if (coordinatorUrl.CompareTo(getURL()) != 0 && coordinatorUrl.CompareTo(getBackupURL()) != 0){
                        coordinator.joinTx(txId, url);
                    }

                    //add to partcipant
                    if (!participantTransactions.Contains(txId))
                        participantTransactions.Add(txId);

                    if(underTx){
                        Monitor.Enter(padintsCreatedUndexTx);
                        List<UInt32> padints;
                        if(padintsCreatedUndexTx.TryGetValue(txId, out padints)){
                            padints.Add(uid);
                        }else{
                            padints = new List<UInt32>();
                            padints.Add(uid);
                            padintsCreatedUndexTx.Add(txId, padints);
                        }
                        Monitor.Exit(padintsCreatedUndexTx);
                    }
                }
                else{
                    #if DEBUG
                    Console.WriteLine("[Storage] Creating padint " + uid + " FAILED. This Padint exists already.\n");
                    #endif
                    coordinator.markTxFailed(txId);
                    return false;
                }          
            }
            catch (Exception) {
                throw new Exception("[Storage] Cannot create padint " + uid);
            }
            finally{
                Monitor.Exit(storedObjects);
                endWorkBatch();
            }

            #if DEBUG
            Console.WriteLine("[Storage] Creating padint " + uid + " SUCCEED");
            #endif

            //replicate
            if (!isBackup && hasBackup)
                return pairServer.CreatePadInt(uid, txId, numberOfOperations, coordinatorUrl, underTx);

            return true;
        }

        public void WritePadInt(uint uid, ulong txId, ulong numberOfOperations, int value, string coordinatorUrl) {

            if (!isBackup && !hasBackup)
            {
                #if DEBUG
                Console.WriteLine("[Storage] Cannot guarantee fault tolerance.");
                #endif
            }
            
            checkPadintMigration(uid);

            //Check if the request's response is cached
            Monitor.Enter(requests);
            Dictionary<ulong, int> operations;
            if (requests.TryGetValue(txId, out operations))
            {
                int cachedResponse;
                if (operations.TryGetValue(numberOfOperations, out cachedResponse))
                    return;
            }
            else{
                //first time this tx is seen
                operations = new Dictionary<ulong,int>();
                requests.Add(txId, operations);
            }
            Monitor.Exit(requests);


            ServerIF coordinator = getServerRef(coordinatorUrl);

            #if DEBUG
            Console.WriteLine("[Storage] Tx:" + txId + ", Writing padint " + uid + " with value " + value);
            #endif

            if (!checkInternalStatus()) {
                coordinator.markTxFailed(txId);
                return;
            }
            if (!CanContinue(txId))
                throw new RequestDeniedException();

            PadIntCapsule padint = null;

            UInt64 hash = PadiDstmUtilities.GetHash(uid.ToString());
            #if DEBUG
            Console.WriteLine("[Storage] Hash = " + hash);
            #endif
            Monitor.Enter(storedObjects);
            if (!storedObjects.Contains(hash))
            {
                Monitor.Exit(storedObjects);
                endWorkBatch();
                #if DEBUG
                Console.WriteLine("[Storage] Throwing exception because there is no such Padint.");
                #endif
                throw new Exception("[Storage] Padint " + uid + " does not exist");
            }

            padint = storedObjects[hash];

            //If this server isn't the coordinator
            if (coordinatorUrl.CompareTo(getURL()) != 0 && coordinatorUrl.CompareTo(getBackupURL()) != 0)
                coordinator.joinTx(txId, url);

            //add to partcipant
            if(!participantTransactions.Contains(txId))
                participantTransactions.Add(txId);

            
            try {
                coordinator.addPadintPartcipant(txId, uid);
                padint.write(value, txId);
            }
            catch(WriteFailedException) {
                
                #if DEBUG
                System.Console.WriteLine("[Storage] WriteFailedException catched.");
                #endif
                coordinator.markTxFailed(txId);
            }
            finally{
                Monitor.Exit(storedObjects);
                endWorkBatch();
            }

            //a new request was done
            Monitor.Enter(requests);
            operations.Add(numberOfOperations, 0);
            Monitor.Exit(requests);

            //replicate
            if (!isBackup && hasBackup)
                pairServer.WritePadInt(uid, txId, numberOfOperations, value, coordinatorUrl);

        }

        public int ReadPadInt(uint uid, ulong txId, ulong numberOfOperations, string coordinatorUrl) {

            int response;

            if (!isBackup && !hasBackup){
                #if DEBUG
                Console.WriteLine("[Storage] Cannot guarantee fault tolerance.");
                #endif
            }

            checkPadintMigration(uid);

            //Check if the request's response is cached
            Monitor.Enter(requests);
            Dictionary<ulong, int> operations;
            if (requests.TryGetValue(txId, out operations)){
                int cachedResponse;
                if (operations.TryGetValue(numberOfOperations, out cachedResponse))
                    return cachedResponse;
            }
            else{
                //first time this tx is seen
                operations = new Dictionary<ulong, int>();
                requests.Add(txId, operations);
            }
            Monitor.Exit(requests);


            ServerIF coordinator = getServerRef(coordinatorUrl);

            #if DEBUG
            Console.WriteLine("[Storage] Tx:" + txId + ", Reading padint " + uid);
            #endif

            if (!checkInternalStatus()) {
                coordinator.markTxFailed(txId);
                return 0;
            }
            if (!CanContinue(txId))
                throw new RequestDeniedException();

            PadIntCapsule padint = null;

            Monitor.Enter(storedObjects);

            if (!storedObjects.Contains(PadiDstmUtilities.GetHash(uid.ToString())))
            {
                Monitor.Exit(storedObjects);
                endWorkBatch();
                throw new Exception("[Storage] Padint " + uid + " does not exist");
            }

            padint = storedObjects[PadiDstmUtilities.GetHash(uid.ToString())];

            //If this server isn't the coordinator
            if (coordinatorUrl.CompareTo(getURL()) != 0 && coordinatorUrl.CompareTo(getBackupURL()) != 0)
                coordinator.joinTx(txId, url);

            //add to partcipant
            if(!participantTransactions.Contains(txId))
                participantTransactions.Add(txId);

            try {
                coordinator.addPadintPartcipant(txId, uid);
                int value = padint.read(txId);
                #if DEBUG
                Console.WriteLine("[Storage] Tx:" + txId + ", was able to read padint " + uid + " and has value " + value);
                #endif
                response = value;
            }
            catch(ReadFailedException) {

                #if DEBUG
                System.Console.WriteLine("[Storage] ReadFailedException catched.");
                #endif
                
                coordinator.markTxFailed(txId);
                return 0;
            }
            finally{
                Monitor.Exit(storedObjects);
                endWorkBatch();
            }

            //a new request was done
            Monitor.Enter(requests);
            operations.Add(numberOfOperations, response);
            Monitor.Exit(requests);

            //replicate
            if (!isBackup && hasBackup)
                response = pairServer.ReadPadInt(uid, txId, numberOfOperations, coordinatorUrl);

            return response;
        }

        public bool checkInternalStatus() {
            Monitor.Enter(statusLock);
            bool res = status != serverState.Fail;
            if (status == serverState.Freeze) {
                Monitor.Wait(statusLock);
            }
            Monitor.Exit(statusLock);
            return res;
        }

        public void endWorkBatch() {
            Monitor.Enter(statusLock);
            if (status == serverState.Alive) {
                Monitor.Pulse(statusLock);
            }
            Monitor.Exit(statusLock);
        }

        public void printStatus() {
            System.Console.WriteLine("------------------------\n----Server's Status-----\n------------------------");
            String report = "Name: " + url;
            report += "\nDuty: ";
            if (isBackup){
                report += "Backup Server";
                report += "\nPair server: Primary running at " + pairUrl;
            }
            else{
                report += "Primary Server";
                if (hasBackup)
                    report += "\nPair server: Backup running at " + pairUrl;
                else
                    report += "\nPair server: none";
            }
            Monitor.Enter(statusLock);
            report += "\nState: " + status;
            Monitor.Exit(statusLock);
            report += "\nStored Objects(uid): ";
            Monitor.Enter(storedObjects);
            foreach (C5.KeyValuePair<UInt64, PadIntCapsule> pair in storedObjects) {
                report += pair.Value.UID + " ";
            }
            Monitor.Exit(storedObjects);
            report += "\nCoordinating Transactions: ";
            Monitor.Enter(transactions);
            foreach (System.Collections.Generic.KeyValuePair<ulong, Transaction> pair in transactions) {
                report += pair.Value.id + " ";
            }
            Monitor.Exit(transactions);
            report += "\nParticipating Transacions: ";
            Monitor.Enter(participantTransactions);
            foreach (ulong id in participantTransactions) {
                report += id + " ";
            }
            Monitor.Exit(participantTransactions);
            report += "\n------------------------\nPadints:\n";
            Console.Write(report);
            Monitor.Enter(storedObjects);
            foreach(C5.KeyValuePair<UInt64, PadIntCapsule> pair in storedObjects) {
                pair.Value.dump();
                Console.WriteLine("");
            }
            Monitor.Exit(storedObjects);

            Monitor.Enter(padintsCreatedUndexTx);
            foreach (var item in padintsCreatedUndexTx)
            {
                Console.WriteLine("\nPadInts created within Tx_"+ item.Key + ": ");
                foreach (var itemNum in item.Value)
                {
                    Console.WriteLine("- "+ itemNum);
                }
            }
            Monitor.Exit(padintsCreatedUndexTx);
            Console.WriteLine("------------------------");

            if (hasBackup)
                pairServer.printStatus();
        }

        public void Fail() {

            if (!checkInternalStatus()) {
                return;
            }

            #if DEBUG
            Console.WriteLine("[DEBUG] Fail called");
            #endif
            Monitor.Enter(statusLock);
            /* We can't call endWorkBatch() after unlocking:
             * if Freeze() is concurrently invoked before calling endWorkBatch(),
             * we could be Pulse()'ing from a freeze status illegally.
             * We also can't call endWorkBatch() before unlocking, because endWorkBatch()
             * tries to lock statusLock, so we have to manually test for this condition inside
             * this code block.
             */
            serverState oldState = status;
            status = serverState.Fail;
            if (oldState == serverState.Alive) {
                Monitor.Pulse(statusLock);
            }
            Monitor.Exit(statusLock);
        }

        public void Freeze() {
            if (!checkInternalStatus()) {
                return;
            }

            #if DEBUG
            Console.WriteLine("[DEBUG] Freeze called");
            #endif
            Monitor.Enter(statusLock);
            /* We can't call endWorkBatch() after unlocking:
             * if Freeze() is concurrently invoked before calling endWorkBatch(),
             * we could be Pulse()'ing from a freeze status illegally.
             * We also can't call endWorkBatch() before unlocking, because endWorkBatch()
             * tries to lock statusLock, so we have to manually test for this condition inside
             * this code block.
             */
            serverState oldState = status;
            if (oldState == serverState.Alive) {
                Monitor.Pulse(statusLock);
            }
            status = serverState.Freeze;
            Monitor.Exit(statusLock);
        }

        public void Recover() {

            #if DEBUG
            Console.WriteLine("[DEBUG] Recover called");
            #endif

            Monitor.Enter(statusLock);
            serverState oldStatus = status;
            status = serverState.Alive;
            if (oldStatus == serverState.Freeze) {
                Monitor.Pulse(statusLock);
            }
            Monitor.Exit(statusLock);
        }

        public string getURL(){
            return url;
        }

        public string getBackupURL(){
            return pairUrl;
        }

        #endregion

        #region Coordinator

        public void openTx(ulong txId) {
            Transaction tx = new Transaction(txId);

            Monitor.Enter(transactions);
            transactions.Add(txId, tx);

            //This server is the first server to be contacted in the context of the TX, which make him the coordinator
            tx.AddParticipant(this);

            Monitor.Exit(transactions);

            // replicate to backup
            if (!isBackup && hasBackup)
                pairServer.openTx(txId);
        }

        public void noDutyOnTx(ulong txId){
            Transaction tx;
            Monitor.Enter(transactions);
            transactions.TryGetValue(txId, out tx);
            tx.RemoveParticipant(this);
            Monitor.Exit(transactions);

            if (!isBackup && hasBackup)
                pairServer.noDutyOnTx(txId);
        }

        public void markTxFailed(ulong txId) {
            Transaction tx = null;

            Monitor.Enter(transactions);
            if(transactions.TryGetValue(txId, out tx))
                tx.failed = true;
            Monitor.Exit(transactions);

            if (!isBackup && hasBackup)
                pairServer.markTxFailed(txId);
        }

        /*returns true if the coordinator was able to commit the transaction, otherwise it returns false
         */
        public bool closeTx(ulong txId) {
            Transaction tx = null;
            bool result;

            Monitor.Enter(transactions);
            if (transactions.TryGetValue(txId, out tx))
            {
                //just the primary can notify
                if(!isBackup)
                    tx.notifyCommit();
                transactions.Remove(txId);
                result = !tx.failed;
            }
            else
                result = false;
            Monitor.Exit(transactions);

            //replicate to backup
            if (!isBackup && hasBackup){
                bool pairResult = pairServer.closeTx(txId);
                return (pairResult && result);
            }
            else
               return result;
        }

        /*returns true if the coordinator was able to abort the transaction, otherwise it returns false
         */
        public bool abortTx(ulong txId) {            
            Transaction tx = null;
            bool result; 

            Monitor.Enter(transactions);
            if (transactions.TryGetValue(txId, out tx)){
                //replicate to backup
                if(!isBackup)
                    tx.notifyAbort();
                result = true;
            }
            else
                result = false;

            transactions.Remove(txId);
            Monitor.Exit(transactions);

            //replicate to backup
            if (!isBackup && hasBackup){
                bool pairResult = pairServer.abortTx(txId);
                return (pairResult && result);
            }
            else
                return result;
        }

        public void joinTx(ulong txId, string participantUrl) {
           
            Transaction tx = null;

            ServerIF participant = getServerRef(participantUrl);

            Monitor.Enter(transactions);
            if (transactions.TryGetValue(txId, out tx))
                tx.AddParticipant(participant);
            Monitor.Exit(transactions);

            //replicate to backup
            if (!isBackup && hasBackup)
                pairServer.joinTx(txId, participantUrl);
        }

        public void addPadintPartcipant(ulong txId, UInt32 padint) {
            Transaction tx = null;

            Monitor.Enter(transactions);
            if (transactions.TryGetValue(txId, out tx))
                tx.AddPadintPartcipant(padint);
            Monitor.Exit(transactions);

            //replicate to backup
            if (!isBackup && hasBackup)
                pairServer.addPadintPartcipant(txId, padint);
        }

        #endregion

        #region Two Phase Commit

        public bool canCommit(ulong txId) {
            Monitor.Enter(participantTransactions);
            bool ret = participantTransactions.Contains(txId);
            Monitor.Exit(participantTransactions);

            //replicate 
            if (!isBackup && hasBackup){
                bool retBackup = pairServer.canCommit(txId);
                return (retBackup && ret);
            }
            else
                return ret;
        }

        public void doCommit(ulong txId, List<UInt32> padintParticipants) {

            #if DEBUG
            Console.WriteLine("[Coordination] Commiting Tx: " + txId);
            #endif

            Monitor.Enter(storedObjects);
            Monitor.Enter(padintsCreatedUndexTx);
            foreach (UInt32 padint in padintParticipants) {
                UInt64 hash = PadiDstmUtilities.GetHash(padint.ToString());
                if (storedObjects.Contains(hash))
                    storedObjects[hash].commit(txId);
            }
            padintsCreatedUndexTx.Remove(txId);
            Monitor.Exit(padintsCreatedUndexTx);
            Monitor.Exit(storedObjects);

            //cleaning response to requests to a given TX
            Monitor.Enter(requests);
            requests.Remove(txId);
            Monitor.Exit(requests);

            //replicate
            if (!isBackup && hasBackup) {
                pairServer.doCommit(txId, padintParticipants);
            }

            CheckIsLast(txId);
        }

        public void doAbort(ulong txId, List<UInt32> padintParticipants) {

            #if DEBUG
            Console.WriteLine("[Coordination] Aborting Tx: " + txId);
            #endif

            Monitor.Enter(storedObjects);
            Monitor.Enter(padintsCreatedUndexTx);
            foreach (UInt32 padint in padintParticipants) {
                UInt64 hash = PadiDstmUtilities.GetHash(padint.ToString());
                if (storedObjects.Contains(hash))
                    storedObjects[hash].abort(txId);
            }
            List<UInt32> padints;
            if(padintsCreatedUndexTx.TryGetValue(txId, out padints))
                foreach (UInt32 item in padints)
                {
                    storedObjects.Remove(PadiDstmUtilities.GetHash(item.ToString()));
                }
            padintsCreatedUndexTx.Remove(txId);
            Monitor.Exit(padintsCreatedUndexTx);
            Monitor.Exit(storedObjects);

            //cleaning response to requests to a given TX
            Monitor.Enter(requests);
            requests.Remove(txId);
            Monitor.Exit(requests);

            //replicate
            if (!isBackup && hasBackup) {
                pairServer.doAbort(txId, padintParticipants);
            }

            CheckIsLast(txId);
        }

        private void CheckIsLast(ulong txId)
        {
            #if DEBUG
            Console.WriteLine("[Coordination] Entered CheckIsLast()");
            #endif
            Monitor.Enter(participantTransactions);

            if (participantTransactions.Contains(txId))
                participantTransactions.Remove(txId);

            if (participantTransactions.Count == 0)
                Monitor.Pulse(participantTransactions);
            Monitor.Exit(participantTransactions);
        }

        public void haveCommitedAll(ulong txId, ServerIF participant) {
            return; //TODO - not needed?
        }

        public bool getDecision(ulong txId) {
            return false; //TODO - not needed?
        }

        #endregion

        #region Auxiliar Functions

        private ServerIF getServerRef(string url){
            return ((ServerIF)Activator.GetObject(typeof(ServerIF), url));
        }

        private MasterIF getMasterRef(){
            return ((MasterIF)Activator.GetObject(typeof(MasterIF), PadiDstmUtilities.MSLOCATION));
        }

        private void checkPadintMigration(uint uid)
        {
            Monitor.Enter(transferedPadints);
            if (transferedPadints.ContainsKey(uid))
            {
                Monitor.Exit(transferedPadints);
                throw new PadintMovedException(transferedPadints[uid].NewServer);
            }
            Monitor.Exit(transferedPadints);
        }

        #endregion

    }

    public class Transaction {
        
        public ulong id;

        public bool failed;

        private List<ServerIF> txParticipants;

        public List<UInt32> padintParticipants;

        public Transaction(ulong tx) {
            this.id = tx;
            failed = false;
            padintParticipants = new List<UInt32>();
            txParticipants = new List<ServerIF>();
        }

        public void notifyCommit() {

            //transaction already failed before
            if(failed) {
                notifyAbort();
                return;
            }

            //check if every server agrees
            foreach (ServerIF participant in txParticipants) {
                if(!participant.canCommit(id)) {
                    failed = true;
                    break;
                }
            }
            if(failed) {
                notifyAbort();
            }
            else {
                foreach(ServerIF participant in txParticipants) {
                    participant.doCommit(id, padintParticipants);
                }
            }
        }

        public void notifyAbort() {
            foreach (ServerIF participant in txParticipants){
                participant.doAbort(id, padintParticipants);
            }
        }

        public void AddParticipant(ServerIF participant) {
            if(!txParticipants.Contains(participant))
                txParticipants.Add(participant);
        }

        public void RemoveParticipant(ServerIF participant){
            if (txParticipants.Contains(participant))
                txParticipants.Remove(participant);
        }

        public void AddPadintPartcipant(UInt32 padintId) {
            padintParticipants.Add(padintId);
        }

        private ServerIF getServerRef(string url)
        {
            return ((ServerIF)Activator.GetObject(typeof(ServerIF), url));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PadiDstmCommonTypes;
using System.Diagnostics;

namespace Server
{
    class TransferedPadint
    {
        //private const long lifespan = 300 * 1000; // 5 minutes

        private uint uid;
        private ServerIF newServer;
        //private Stopwatch expirationWatch;

        public TransferedPadint(uint padintID, ServerIF newServer)
        {
            uid = padintID;
            this.newServer = newServer;
            //expirationWatch = new Stopwatch();
            //expirationWatch.Start();
        }
        /*
        public bool Expired()
        {
            if (expirationWatch.ElapsedMilliseconds >= lifespan)
            {
                expirationWatch.Stop();
                return true;
            }
            return false;
        }*/
        public ServerIF NewServer
        {
            get { return newServer; }
        }
    }
}

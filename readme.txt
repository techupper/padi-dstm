To launch the servers, one should run the script runProj.bat after building the 
solution (the paths of the exe files assume the solution were built in debug mode, 
otherwise the script won't work.) 

To change the number of servers, one need to specify a lower and an upper bound that 
are going to define the range of ports where the servers will be listening.

The solution was developed using the Visual Studio 2013.
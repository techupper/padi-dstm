@echo off
set lowerJoinPort=2001
set upperJoinPort=2010
set wait=ping 1.1.1.1 -n 1 -w 1000 > nul
start Master\bin\Debug\MasterServer.exe
%wait%
for /l %%x in (%lowerJoinPort%, 1,%upperJoinPort%) do (
	ECHO Going to launch another server on port %%x
	set /p launch=Enter to launch:
	start Server\bin\Debug\Server.exe %%x
	%wait%
)

